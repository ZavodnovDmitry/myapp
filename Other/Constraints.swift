//
//  Constraints.swift
//  MyApp
//
//  Created by Дмитрий Заводнов on 5/30/20.
//  Copyright © 2020 Дмитрий Заводнов. All rights reserved.
//

import Foundation
import UIKit

struct Constraints {
    
    static let zero = CGFloat(0)
    
    struct GalleryConstraints {
        static let contentInsetLeft = CGFloat(10)
        static let contentInsetRight = CGFloat(10)
        static let minimumLineSpacing = CGFloat(10)
        static let galleryItemWidth = (UIScreen.main.bounds.width - GalleryConstraints.contentInsetLeft - GalleryConstraints.contentInsetRight - (GalleryConstraints.minimumLineSpacing / 2)) / 2
        static let galleryItemHeight = UIScreen.main.bounds.height * 0.5
    }
}
