//
//  ExtensionString.swift
//  MyApp
//
//  Created by Дмитрий Заводнов on 5/29/20.
//  Copyright © 2020 Дмитрий Заводнов. All rights reserved.
//

import Foundation

extension String {
    
    struct GalleryString {
        static let titleGalleryVC = "Gallery"
        static let imageGalleryVC = "square.stack"
        static let notesVCTitle = "notes"
        static let predictVCTitle = "predict"
        static let horoscopeVCTitle = "horoscope"
        static let phoneVCTitle = "phone"
    }
}
