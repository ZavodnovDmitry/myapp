//
//  NotebookTextView.swift
//  MyApp
//
//  Created by Дмитрий Заводнов on 6/4/20.
//  Copyright © 2020 Дмитрий Заводнов. All rights reserved.
//

import Foundation
import UIKit

final class NotebookTextView: UITextView {
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)

        text = ""
        layer.cornerRadius = 12
        backgroundColor = .white
        font = UIFont.systemFont(ofSize: 14)
    }
}

