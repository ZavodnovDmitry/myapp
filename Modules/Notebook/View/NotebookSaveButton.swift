//
//  NotebookSaveButton.swift
//  MyApp
//
//  Created by Дмитрий Заводнов on 6/4/20.
//  Copyright © 2020 Дмитрий Заводнов. All rights reserved.
//

import Foundation
import UIKit

final class SaveNotebookViewButton: UIButton {
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)

        setTitle("Save", for: .normal)
        backgroundColor = .systemOrange
        layer.cornerRadius = 12
        layer.borderColor = UIColor.white.cgColor
        layer.borderWidth = 1
    }
}
