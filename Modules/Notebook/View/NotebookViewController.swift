//
//  NotebookViewController.swift
//  MyApp
//
//  Created by Дмитрий Заводнов on 6/2/20.
//  Copyright © 2020 Дмитрий Заводнов. All rights reserved.
//

import UIKit

final class NotebookViewController: UIViewController {
    var notebookViewModel: NotebookViewModelProtocol?
    var dataBaseService: DataBaseServiceProtocol?
    private var textModels = [NotesModel]()
    private var writenText = String()
    
    @IBOutlet private weak var tableView: UITableView!
    @IBOutlet private weak var writeButton: UIButton!
    @IBOutlet private weak var writeView: UIView!
    @IBOutlet private weak var saveButton: UIButton!
    @IBOutlet private weak var textView: UITextView!
    @IBOutlet private weak var writeViewBottomConstraint: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        settingsForWriteButton()
        settingsForWriteView()
        settingsForTableView()
        addTargetsAreButtons()
        notificationForkeyboardPosition()
        getDataFromTheDataBase()
    }
    
    static func instantiateInitialVC() -> Self {
        guard let result = UIStoryboard(name: "NotebookTableView", bundle: nil).instantiateInitialViewController() as? Self else {
            fatalError()
        }
        return result
    }
    
    private func getDataFromTheDataBase() {
        guard let dataBaseService = dataBaseService else { return }
        self.textModels = dataBaseService.obtainTexts()
    }
    
    //MARK: - Setup settings for UI
    private func settingsForWriteButton() {
        let pencil = UIImage(systemName: "pencil.circle")
        writeButton.setBackgroundImage(pencil, for: .normal)
    }
    
    private func settingsForWriteView() {
        writeView.layer.cornerRadius = 12
        writeView.backgroundColor = .darkGray
        writeView.isHidden = true
    }
    
    // MARK: - Settings TableView
    private func settingsForTableView() {
        tableView.backgroundColor = .black
        tableView.alpha = 0.8
    }
    
    
    //MARK: - Actions Buttons
    private func addTargetsAreButtons() {
        writeButton.addTarget(self, action: #selector(pressWriteNotebookButton), for: .touchDown)
        saveButton.addTarget(self, action: #selector(pressSaveNotebookViewButton), for: .touchDown)
    }
    
    @objc func pressWriteNotebookButton() {
        writeView.isHidden = false
    }
    
    @objc func pressSaveNotebookViewButton() {
        self.writeView.isHidden = true
        
        if textView.text != "" {
            notebookViewModel?.saveTextInTextModel(text: self.writenText)
            textView.text = ""
        }
        guard let notebookViewModel = notebookViewModel else { return }
        self.textModels = notebookViewModel.obtainTextInDataBase()
        writeViewBottomConstraint.constant = 0
        tableView.reloadData()
    }
    
    //MARK: - Notification for keyboard
    private func notificationForkeyboardPosition() {
        NotificationCenter.default.addObserver(self, selector: #selector(updateWriteView), name: UIResponder.keyboardDidShowNotification, object: nil)
    }
     
    @objc func updateWriteView(param: Notification) {
         let userInfo = param.userInfo
         
         let getKeyboardRect = (userInfo![UIResponder.keyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
         let keyboardFrame = self.view.convert(getKeyboardRect, to: view.window)
         
         writeViewBottomConstraint.constant = keyboardFrame.height - 60
     }

}

// MARK: - UITableViewDelegate, UITableViewDataSource
extension NotebookViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return textModels.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "notebookCell", for: indexPath) as? NotebookTableViewCell {
            
            let textModel = textModels[indexPath.row]
            cell.userText.text = textModel.text
            
            return cell
        }
        return UITableViewCell()
    }
    
}

//MARK: - Delete cell in TableView
extension NotebookViewController {
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        return .delete
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            
            let removeTextModels = textModels.remove(at: indexPath.row)
            notebookViewModel?.removeTextInDataBase(text: removeTextModels)
            
            tableView.deleteRows(at: [indexPath], with: .left)
        }
    }
    
}

// MARK: - UITextViewDelegate
extension NotebookViewController: UITextViewDelegate {
    func textViewDidChange(_ textView: UITextView) {
        if let text = textView.text {
            self.writenText = text
        } else {
            return
        }
    }
    
}

