//
//  NotebookTableViewCell.swift
//  MyApp
//
//  Created by Дмитрий Заводнов on 6/4/20.
//  Copyright © 2020 Дмитрий Заводнов. All rights reserved.
//

import UIKit

class NotebookTableViewCell: UITableViewCell {
    @IBOutlet weak var userText: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        backgroundColor = .black
        alpha = 0.75
        userText.textColor = .white
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
