//
//  DataBaseService.swift
//  MyApp
//
//  Created by Дмитрий Заводнов on 6/3/20.
//  Copyright © 2020 Дмитрий Заводнов. All rights reserved.
//

import Foundation
import RealmSwift

final class DataBaseService: DataBaseServiceProtocol {
    private let realm = try! Realm()
    
    func saveText(text: NotesModel) {
        do {
            try self.realm.write {
                self.realm.add(text)
            }
        } catch let error {
            print("saveText error: ", error)
        }
    }
    
    func obtainTexts() -> [NotesModel] {
        let models = realm.objects(NotesModel.self)
        return Array(models)
    }
    
    func removeText(text: Object) {
        do {
            try realm.write {
                realm.delete(text)
            }
        } catch let error {
            print("removeText error: ", error)
        }
    }
    
    func removeAllTexts() {
        do {
            try realm.write {
                realm.deleteAll()
            }
        } catch let error {
            print("removeAllText error: ", error)
        }
    }
}



