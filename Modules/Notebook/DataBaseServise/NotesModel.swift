//
//  NotesModel.swift
//  MyApp
//
//  Created by Дмитрий Заводнов on 6/3/20.
//  Copyright © 2020 Дмитрий Заводнов. All rights reserved.
//

import Foundation
import RealmSwift

@objcMembers
final class NotesModel: Object {
    
    dynamic var text = String()
    dynamic var date = String()
    dynamic var id = String()
    
    override class func primaryKey() -> String? {
        return #keyPath(id)
    }
    
}
