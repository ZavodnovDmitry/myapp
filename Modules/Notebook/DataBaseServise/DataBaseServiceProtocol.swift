//
//  DataBaseServiceProtocol.swift
//  MyApp
//
//  Created by Дмитрий Заводнов on 6/3/20.
//  Copyright © 2020 Дмитрий Заводнов. All rights reserved.
//

import Foundation
import RealmSwift

protocol DataBaseServiceProtocol {
    func saveText(text: NotesModel)
    func obtainTexts() -> [NotesModel]
    func removeText(text: Object)
    func removeAllTexts()
}
