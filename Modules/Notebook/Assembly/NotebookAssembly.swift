//
//  NotebookAssembly.swift
//  MyApp
//
//  Created by Дмитрий Заводнов on 6/5/20.
//  Copyright © 2020 Дмитрий Заводнов. All rights reserved.
//

import Foundation
import UIKit

final class NotebookAssembly {
    func assemblyNotebook() -> NotebookViewController {
        let dataBaseService =  DataBaseService()
        let notebookViewModel = NotebookViewModel(dataBaseService: dataBaseService)
        let notebookVC = NotebookViewController.instantiateInitialVC()
        notebookVC.notebookViewModel = notebookViewModel
        notebookVC.dataBaseService = dataBaseService
        
        return notebookVC
    }
    
}
