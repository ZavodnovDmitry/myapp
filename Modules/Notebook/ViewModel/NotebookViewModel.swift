//
//  NotebookViewModel.swift
//  MyApp
//
//  Created by Дмитрий Заводнов on 6/3/20.
//  Copyright © 2020 Дмитрий Заводнов. All rights reserved.
//

import Foundation
import RealmSwift

final class NotebookViewModel: NotebookViewModelProtocol  {
    private let dataBaseService: DataBaseServiceProtocol
    private let userDefaults = UserDefaults.standard
    
    init(dataBaseService: DataBaseServiceProtocol) {
        self.dataBaseService = dataBaseService
    }
    
    func saveTextInTextModel(text: String) {
        let saveIdTextModel = userDefaults.integer(forKey: "idTextModel")
        let textModel = NotesModel()
        let date = getDate()
        textModel.id = "\(saveIdTextModel)"
        textModel.text = text
        textModel.date = date
        userDefaults.set(saveIdTextModel + 1, forKey: "idTextModel")
        
        dataBaseService.saveText(text: textModel)
    }
    
    func obtainTextInDataBase() -> [NotesModel] {
        return dataBaseService.obtainTexts()
    }
    
    func removeTextInDataBase(text: Object) {
        dataBaseService.removeText(text: text)
    }
    
    private func getDate() -> String {
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        formatter.locale = Locale(identifier: "en_US_POSIX")
        let dateStr = formatter.string(from: date)
        return dateStr
    }
    
}
