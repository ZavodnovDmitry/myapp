//
//  NotebookViewModelProtocol.swift
//  MyApp
//
//  Created by Дмитрий Заводнов on 6/3/20.
//  Copyright © 2020 Дмитрий Заводнов. All rights reserved.
//

import Foundation
import RealmSwift

protocol NotebookViewModelProtocol {
    func obtainTextInDataBase() -> [NotesModel]
    func removeTextInDataBase(text: Object)
    func saveTextInTextModel(text: String)
}
