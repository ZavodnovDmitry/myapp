//
//  PhoneCellViewModel.swift
//  MyApp
//
//  Created by Дмитрий Заводнов on 6/23/20.
//  Copyright © 2020 Дмитрий Заводнов. All rights reserved.
//

struct PhoneCellViewModel {
    let firstname: String
    let lastname: String
    let phonenumber: String
    let id: Int
}

extension PhoneCellViewModel {
    init(phone: Phone) {
        self.firstname = phone.firstname
        self.lastname = phone.lastname
        self.phonenumber = phone.phonenumber
        self.id = phone.id
    }
}

