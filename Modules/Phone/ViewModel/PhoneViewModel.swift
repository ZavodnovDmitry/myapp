//
//  PhoneViewModel.swift
//  MyApp
//
//  Created by Дмитрий Заводнов on 6/23/20.
//  Copyright © 2020 Дмитрий Заводнов. All rights reserved.
//

import RxSwift
import RxCocoa

protocol PhoneViewModelProtocol {
    var phoneCells: Observable<[PhoneTableViewCellType]> { get }
    var onShowError: PublishSubject<SingleButtonAlert> { get }
    var onShowLoadingHud: Observable<Bool> { get }
    
    func getPhones()
    func delete(phone: PhoneCellViewModel)
}

enum PhoneTableViewCellType {
    case normal(cellViewModel: PhoneCellViewModel)
    case error(message: String)
    case empty
}

final class PhoneViewModel: PhoneViewModelProtocol {
    private let appServerClient: AppServerClientProtocol
    private let loadInProgress = BehaviorRelay(value: false)
    private let cells = BehaviorRelay<[PhoneTableViewCellType]>(value: [])
    private let disposeBag = DisposeBag()
    
    let onShowError = PublishSubject<SingleButtonAlert>()
    
    init(appServerClient: AppServerClientProtocol) {
        self.appServerClient = appServerClient
    }
    
    var phoneCells: Observable<[PhoneTableViewCellType]> {
        return cells.asObservable()
    }
    var onShowLoadingHud: Observable<Bool> {
        return loadInProgress
            .asObservable()
            .distinctUntilChanged()
    }
    
    func getPhones() {
        loadInProgress.accept(true)
        
        appServerClient
            .getPhones()
            .subscribe(
                onNext: { [weak self] phones in
                    self?.loadInProgress.accept(false)
                    guard phones.count > 0 else {
                        self?.cells.accept([.empty])
                        return
                    }
                    self?.cells.accept(phones.compactMap { .normal(cellViewModel: PhoneCellViewModel(phone: $0 )) })
                },
                onError: { [weak self] error in
                    self?.loadInProgress.accept(false)
                    self?.cells.accept([
                        .error(
                            message: (error as? AppServerClient.GetPhonesFailureReason)?.getErrorMessage() ?? "Loading failed, check network connection"
                        )
                    ])
                }
        )
            .disposed(by: disposeBag)
    }
    
    func delete(phone: PhoneCellViewModel) {
        appServerClient
            .deletePhones(id: phone.id)
            .subscribe(
                onNext: { [weak self] phones in
                    self?.getPhones()
                },
                onError: { [weak self] error in
                    let okAlert = SingleButtonAlert(
                        title: (error as? AppServerClient.DeletePhonesFailureReason)?.getErrorMessage() ?? "Could not connect to server. Check your network and try again later.",
                        message: "Could not remove \(phone.firstname) \(phone.lastname).",
                        action: AlertAction(buttonTitle: "OK", handler: { print("Ok pressed!") })
                    )
                    self?.onShowError.onNext(okAlert)
                }
        )
            .disposed(by: disposeBag)
    }
}

// MARK: - AppServerClient get phones
fileprivate extension AppServerClient.GetPhonesFailureReason {
    func getErrorMessage() -> String? {
        switch self {
        case .unAuthorized:
            return "Please login to load your phones."
        case .notFound:
            return "Could not complete request, please try again."
        }
    }
}

// MARK: - AppServerClient delete phones
fileprivate extension AppServerClient.DeletePhonesFailureReason {
    func getErrorMessage() -> String? {
        switch self {
        case .unAuthorized:
            return "Please login to remove phones."
        case .notFound:
            return "Phones not found."
        }
    }
}


