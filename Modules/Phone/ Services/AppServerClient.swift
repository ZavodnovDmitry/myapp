//
//  AppServerClient.swift
//  MyApp
//
//  Created by Дмитрий Заводнов on 6/23/20.
//  Copyright © 2020 Дмитрий Заводнов. All rights reserved.
//

import Alamofire
import RxSwift

protocol AppServerClientProtocol {
    func getPhones() -> Observable<[Phone]>
    func deletePhones(id: Int) -> Observable<Void>
    func postPhones(firstname: String, lastname: String, phonenumber: String) -> Observable<Void>
    func patchPhone(firstname: String, lastname: String, phonenumber: String, id: Int) -> Observable<Phone>
}

final class AppServerClient: AppServerClientProtocol {
    
    enum GetPhonesFailureReason: Int, Error {
        case unAuthorized = 401
        case notFound = 404
    }
    
    enum PostPhonesFailureReason: Int, Error {
        case unAuthorized = 401
        case notFound = 404
    }
    
    enum PatchPhonesFailureReason: Int, Error {
        case unAuthorized = 401
        case notFound = 404
    }
    
    enum DeletePhonesFailureReason: Int, Error {
        case unAuthorized = 401
        case notFound = 404
    }
    
    // MARK: - getPhones
    func getPhones() -> Observable<[Phone]> {
        return Observable.create { observer -> Disposable in
            AF.request("http://friendservice.herokuapp.com/listFriends")
                .validate()
                .responseJSON { response in
                    switch response.result {
                    case .success:
                        guard let data = response.data else {
                            observer.onError(response.error ?? GetPhonesFailureReason.notFound)
                            return
                        }
                        do {
                            let phones = try JSONDecoder().decode([Phone].self, from: data)
                            observer.onNext(phones)
                        } catch {
                            observer.onError(error)
                        }
                    case .failure(let error):
                        if let statusCode = response.response?.statusCode,
                            let reason = GetPhonesFailureReason(rawValue: statusCode)
                        {
                            observer.onError(reason)
                        }
                        observer.onError(error)
                    }
            }
            return Disposables.create()
        }
    }
    
    // MARK: - postPhones
    func postPhones(firstname: String, lastname: String, phonenumber: String) -> Observable<Void> {
        let param = ["firstname": firstname,
                     "lastname": lastname,
                     "phonenumber": phonenumber]
        
        return Observable<Void>.create { [param] observer -> Disposable in
            AF.request("https://friendservice.herokuapp.com/addFriend", method: .post, parameters: param, encoding: JSONEncoding.default)
                .validate()
                .responseJSON { [observer] response in
                    switch response.result {
                    case .success:
                        observer.onNext(())
                    case .failure(let error):
                        if let statusCode = response.response?.statusCode,
                            let reason = PostPhonesFailureReason(rawValue: statusCode)
                        {
                            observer.onError(reason)
                        }
                        observer.onError(error)
                    }
            }
            return Disposables.create()
        }
    }
    
    // MARK: - patchPhones
    func patchPhone(firstname: String, lastname: String, phonenumber: String, id: Int) -> Observable<Phone> {
        let param = ["firstname": firstname,
                     "lastname": lastname,
                     "phonenumber": phonenumber]
        return Observable.create { observer in
            AF.request("https://friendservice.herokuapp.com/editFriend/\(id)", method: .patch, parameters: param, encoding: JSONEncoding.default)
                .validate()
                .responseJSON { response in
                    switch response.result {
                    case .success:
                        do {
                            guard let data = response.data else {
                                observer.onError(response.error ?? GetPhonesFailureReason.notFound)
                                return
                            }
                            let phone = try JSONDecoder().decode(Phone.self, from: data)
                            observer.onNext(phone)
                        } catch {
                            observer.onError(error)
                        }
                    case .failure(let error):
                        if let statusCode = response.response?.statusCode,
                            let reason = PatchPhonesFailureReason(rawValue: statusCode)
                        {
                            observer.onError(reason)
                        }
                        
                        observer.onError(error)
                    }
            }
            return Disposables.create()
        }
    }
    
    // MARK: - deletePhone
    func deletePhones(id: Int) -> Observable<Void> {
        return Observable.create { observable -> Disposable in
            AF.request("https://friendservice.herokuapp.com/editFriend/\(id)", method: .delete, parameters: nil, encoding: JSONEncoding.default)
                .validate()
                .responseJSON { response in
                    switch response.result {
                    case .success:
                        observable.onNext(())
                    case .failure(let error):
                        if let statusCode = response.response?.statusCode,
                            let reason = DeletePhonesFailureReason(rawValue: statusCode)
                        {
                            observable.onError(reason)
                        }
                        observable.onError(error)
                    }
            }
            return Disposables.create()
        }
    }
}

