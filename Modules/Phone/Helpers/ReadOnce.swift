//
//  ReadOnce.swift
//  MyApp
//
//  Created by Дмитрий Заводнов on 6/23/20.
//  Copyright © 2020 Дмитрий Заводнов. All rights reserved.
//

import Foundation

final class ReadOnce<Value> {
    var isRead: Bool {
        return value == nil
    }

    private var value: Value?

    init(_ value: Value?) {
        self.value = value
    }

    func read() -> Value? {
        defer { value = nil }

        if value != nil {
            return value
        }

        return nil
    }
}

