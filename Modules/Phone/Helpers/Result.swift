//
//  Result.swift
//  MyApp
//
//  Created by Дмитрий Заводнов on 6/23/20.
//  Copyright © 2020 Дмитрий Заводнов. All rights reserved.
//

enum Result<T, U: Error> {
    case success(payload: T)
    case failure(U?)
}

enum EmptyResult<U: Error> {
    case success
    case failure(U?)
}

