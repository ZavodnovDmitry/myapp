//
//  Phone.swift
//  MyApp
//
//  Created by Дмитрий Заводнов on 6/23/20.
//  Copyright © 2020 Дмитрий Заводнов. All rights reserved.
//

struct Phone: Codable {
    let firstname: String
    let lastname: String
    let phonenumber: String
    let id: Int
}
