//
//  PhoneAssembly.swift
//  MyApp
//
//  Created by Дмитрий Заводнов on 6/25/20.
//  Copyright © 2020 Дмитрий Заводнов. All rights reserved.
//

import Foundation

struct PhoneAssembly {
    
    func assemblyPhone() -> PhoneViewController {
        let appServerClient = AppServerClient()
        let viewModel = PhoneViewModel(appServerClient: appServerClient)
        let phoneVC = PhoneViewController.instantiateInitialVC()
        phoneVC.viewModel = viewModel
        phoneVC.appServerClient = appServerClient
        return phoneVC
    }
}
