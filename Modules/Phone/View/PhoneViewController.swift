//
//  PhoneViewController.swift
//  MyApp
//
//  Created by Дмитрий Заводнов on 6/23/20.
//  Copyright © 2020 Дмитрий Заводнов. All rights reserved.
//

import UIKit
import PKHUD
import RxSwift
import RxDataSources

final class PhoneViewController: UIViewController {
    var viewModel: PhoneViewModelProtocol!
    var appServerClient: AppServerClientProtocol!
    
    private let disposeBag = DisposeBag()
    private var selectPhonePayload = ReadOnce<PhoneCellViewModel>(nil)
    
    @IBOutlet private weak var tableView: UITableView!

    // MARK: - Lifecycle
    public override func viewDidLoad() {
        super.viewDidLoad()

        setupBindings()
        setupCellDeleting()
        setupCellTapHandling()

        viewModel.getPhones()
    }
    
    //MARk: - Setup
    static func instantiateInitialVC() -> Self {
        guard let result = UIStoryboard(name: "Phone", bundle: nil).instantiateInitialViewController() as? Self else {
            fatalError()
        }
        return result
    }

    private func setupBindings() {
        viewModel.phoneCells.bind(to: self.tableView.rx.items) { tableView, index, element in
            let indexPath = IndexPath(item: index, section: 0)
            switch element {
            case .normal(let viewModel):
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "phoneCell", for: indexPath) as? PhoneTableViewCell else {
                    return UITableViewCell()
                }
                cell.viewModel = viewModel
                return cell
            case .error(let message):
                let cell = UITableViewCell()
                cell.isUserInteractionEnabled = false
                cell.textLabel?.text = message
                return cell
            case .empty:
                let cell = UITableViewCell()
                cell.isUserInteractionEnabled = false
                cell.textLabel?.text = "no data"
                return cell
            }
        }.disposed(by: disposeBag)

        viewModel
            .onShowError
            .map { [weak self] in self?.presentSingleButtonDialog(alert: $0)}
            .subscribe()
            .disposed(by: disposeBag)

        viewModel
            .onShowLoadingHud
            .map { [weak self] in self?.setLoadingHud(visible: $0) }
            .subscribe()
            .disposed(by: disposeBag)
    }

    private func setLoadingHud(visible: Bool) {
        PKHUD.sharedHUD.contentView = PKHUDSystemActivityIndicatorView()
        visible ? PKHUD.sharedHUD.show(onView: view) : PKHUD.sharedHUD.hide()
    }

    private func setupCellTapHandling() {
        tableView
            .rx
            .modelSelected(PhoneTableViewCellType.self)
            .subscribe(
                onNext: { [weak self] phoneCellType in
                    if case let .normal(viewModel) = phoneCellType {
                        self?.selectPhonePayload = ReadOnce(viewModel)
                        self?.performSegue(withIdentifier: "updatePhone", sender: self)
                    }
                    if let selectedRowIndexPath = self?.tableView.indexPathForSelectedRow {
                        self?.tableView?.deselectRow(at: selectedRowIndexPath, animated: true)
                    }
                }
            )
            .disposed(by: disposeBag)
    }

    private func setupCellDeleting() {
        tableView
            .rx
            .modelDeleted(PhoneTableViewCellType.self)
            .subscribe(
                onNext: { [weak self] phoneCellType in
                    if case let .normal(viewModel) = phoneCellType {
                        self?.viewModel.delete(phone: viewModel)
                    }

                    if let selectedRowIndexPath = self?.tableView.indexPathForSelectedRow {
                        self?.tableView?.deselectRow(at: selectedRowIndexPath, animated: true)
                    }
                }
            )
            .disposed(by: disposeBag)
    }

    public override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        if identifier == "updatePhone" {
            return !selectPhonePayload.isRead
        }

        return super.shouldPerformSegue(withIdentifier: identifier, sender: sender)
    }

    public override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "addPhone",
            let destinationViewController = segue.destination as? AddPhoneViewController
        {
            destinationViewController.viewModel = AddPhoneViewModel(appServerClient: appServerClient)
            destinationViewController.updatePhones.asObserver().subscribe(onNext: { [weak self] () in
                self?.viewModel.getPhones()
                }, onCompleted: {
                    print("onCompleted")
            }).disposed(by: destinationViewController.disposeBag)
        }

        if segue.identifier == "updatePhone",
            let destinationViewController = segue.destination as? AddPhoneViewController,
            let viewModel = selectPhonePayload.read()
        {
            destinationViewController.viewModel = UpdatePhoneViewModel(phoneCellViewModel: viewModel, appServerClient: appServerClient)
            destinationViewController.updatePhones.asObserver().subscribe(onNext: { [weak self] () in
                self?.viewModel.getPhones()
                }, onCompleted: {
                    print("onCompleted")
            }).disposed(by: destinationViewController.disposeBag)
        }
    }
}

// MARK: - SingleButtonDialogPresenter
extension PhoneViewController: SingleButtonDialogPresenter { }
