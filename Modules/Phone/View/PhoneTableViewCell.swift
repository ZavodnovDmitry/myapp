//
//  PhoneTableViewCell.swift
//  MyApp
//
//  Created by Дмитрий Заводнов on 6/23/20.
//  Copyright © 2020 Дмитрий Заводнов. All rights reserved.
//

import UIKit

final class PhoneTableViewCell: UITableViewCell {
    @IBOutlet private weak var fullNameLabel: UILabel!
    @IBOutlet private weak var phoneNumberLabel: UILabel!
    
    var viewModel: PhoneCellViewModel? {
        didSet {
            bindViewModel()
        }
    }

    private func bindViewModel() {
        if let viewModel = viewModel {
            fullNameLabel?.text = "\(viewModel.firstname) \(viewModel.lastname)"
            phoneNumberLabel?.text = viewModel.phonenumber
        }
    }
}
