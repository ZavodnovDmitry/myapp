//
//  HoroscopeViewController.swift
//  MyApp
//
//  Created by Дмитрий Заводнов on 6/18/20.
//  Copyright © 2020 Дмитрий Заводнов. All rights reserved.
//

import UIKit

final class HoroscopeViewController: UIViewController {
    private var collectionView: UICollectionView!
    
    @IBOutlet private weak var collectionViewContainer: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupCollectionView()
        setupCollectionViewConstraints()
    }
    
    static func instantiateInitialVC() -> Self {
        guard let result = UIStoryboard(name: "HoroscopeView", bundle: nil).instantiateInitialViewController() as? Self else {
            fatalError()
        }
        return result
    }
    
    //MARK: - Setup
    private func setupCollectionView() {
        collectionView = UICollectionView(frame: collectionViewContainer.bounds, collectionViewLayout: createGridSection())
        collectionViewContainer.addSubview(collectionView)
        collectionView.backgroundColor = .clear
        collectionView.register(UICollectionViewCell.self, forCellWithReuseIdentifier: "horoscopeCellId")
        collectionView.delegate = self
        collectionView.dataSource = self
    }
    
    private func setupCollectionViewConstraints() {
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        
        collectionView.leadingAnchor.constraint(equalTo: collectionViewContainer.leadingAnchor).isActive = true
        collectionView.trailingAnchor.constraint(equalTo: collectionViewContainer.trailingAnchor).isActive = true
        collectionView.topAnchor.constraint(equalTo: collectionViewContainer.topAnchor).isActive = true
        collectionView.bottomAnchor.constraint(equalTo: collectionViewContainer.bottomAnchor).isActive = true
    }
    
    private func createGridSection() -> UICollectionViewLayout {
        let itemSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1), heightDimension: .fractionalHeight(1))
        
        let layoutItem = NSCollectionLayoutItem(layoutSize: itemSize)
        layoutItem.contentInsets = NSDirectionalEdgeInsets(top: 10, leading: 8, bottom: 0, trailing: 8)
        
        let layoutGroupSize = NSCollectionLayoutSize(widthDimension: .estimated(120), heightDimension: .estimated(120))

        let layoutGroup = NSCollectionLayoutGroup.horizontal(layoutSize: layoutGroupSize, subitems: [layoutItem])
        
        let layoutSection = NSCollectionLayoutSection(group: layoutGroup)
        
        layoutSection.orthogonalScrollingBehavior = .continuous
        layoutSection.contentInsets = NSDirectionalEdgeInsets(top: 20, leading: 12, bottom: 10, trailing: 12)
        
         let layout = UICollectionViewCompositionalLayout(section: layoutSection)
        
        return layout
    }
}

//MARK: - UICollectionViewDataSource, UICollectionViewDelegate
extension HoroscopeViewController: UICollectionViewDataSource, UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 12
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "horoscopeCellId", for: indexPath)
        
        cell.backgroundColor = .systemBlue
        cell.layer.cornerRadius = 5
        cell.layer.borderWidth = 1
        
        return cell
    }
}
