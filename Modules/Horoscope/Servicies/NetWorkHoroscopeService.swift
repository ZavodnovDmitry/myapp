//
//  NetWorkHoroscopeService.swift
//  MyApp
//
//  Created by Дмитрий Заводнов on 6/21/20.
//  Copyright © 2020 Дмитрий Заводнов. All rights reserved.
//

import Foundation

var articles: [Article] {
    do {
        let data = try Data(contentsOf: httpResponse)
        do {
            guard let rootDictionary = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? Dictionary<String, Any> else { return [] }
            if let arrayArticles = rootDictionary["articles"] as? [Dictionary<String, Any>] {
                
                var returnArticleArray: [Article] = []
                
                for dict in arrayArticles {
                    let newsArticle = Article(dictionary: dict)
                    returnArticleArray.append(newsArticle)
                }
                return returnArticleArray
            }
        } catch let error {
            print(error.localizedDescription)
        }
    } catch let error {
        print(error.localizedDescription)
    }
    return []
}

final class NetWorkHoroscopeService {
    let headers = [
        "x-rapidapi-host": "horoscopeapi-horoscope-v1.p.rapidapi.com",
        "x-rapidapi-key": "bbd7a97276msh1494a18003d2e33p1a6b32jsne583ef899166"
    ]
    
    let request = NSMutableURLRequest(url: NSURL(string: "https://horoscopeapi-horoscope-v1.p.rapidapi.com/daily?date=today&sign=taurus")! as URL,
                                      cachePolicy: .useProtocolCachePolicy,
                                      timeoutInterval: 10.0)
    
    func getRequest() {
        request.httpMethod = "GET"
        request.allHTTPHeaderFields = headers
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            if (error != nil) {
                print(error)
            } else {
                let httpResponse = response as? HTTPURLResponse
                print(httpResponse)
            }
        })
        dataTask.resume()
        }
    
    
    }


