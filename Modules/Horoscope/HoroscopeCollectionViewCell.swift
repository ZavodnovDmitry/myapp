//
//  HoroscopeCollectionViewCell.swift
//  MyApp
//
//  Created by Дмитрий Заводнов on 6/19/20.
//  Copyright © 2020 Дмитрий Заводнов. All rights reserved.
//

import UIKit

class HoroscopeCollectionViewCell: UICollectionViewCell {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        backgroundColor = .systemBlue
        layer.cornerRadius = 4
        clipsToBounds = true
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
