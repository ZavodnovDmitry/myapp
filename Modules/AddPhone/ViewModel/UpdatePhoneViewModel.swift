//
//  UpdatePhoneViewModel.swift
//  MyApp
//
//  Created by Дмитрий Заводнов on 6/23/20.
//  Copyright © 2020 Дмитрий Заводнов. All rights reserved.
//

import RxSwift
import RxCocoa

final class UpdatePhoneViewModel: AddPhoneViewModelProtocol {
    let onShowError = PublishSubject<SingleButtonAlert>()
    let onNavigateBack = PublishSubject<Void>()
    let submitButtonTapped = PublishSubject<Void>()
    let disposeBag = DisposeBag()
    
    var title = BehaviorRelay<String>(value: "Update Phone")
    var firstname = BehaviorRelay<String>(value: "")
    var lastname = BehaviorRelay<String>(value: "")
    var phonenumber = BehaviorRelay<String>(value: "")
    var onShowLoadingHud: Observable<Bool> {
        return loadInProgress
            .asObservable()
            .distinctUntilChanged()
    }
    
    var submitButtonEnabled: Observable<Bool> {
        return Observable.combineLatest(firstnameValid, lastnameValid, phoneNumberValid) { $0 && $1 && $2 }
    }
    
    private let appServerClient: AppServerClientProtocol
    private let loadInProgress = BehaviorRelay(value: false)
    private let phoneId: Int
    
    private var firstnameValid: Observable<Bool> {
        return firstname.asObservable().map { $0.count > 0 }
    }
    private var lastnameValid: Observable<Bool> {
        return lastname.asObservable().map { $0.count > 0 }
    }
    private var phoneNumberValid: Observable<Bool> {
        return phonenumber.asObservable().map { $0.count > 0 }
    }
    
    init(phoneCellViewModel: PhoneCellViewModel, appServerClient: AppServerClientProtocol) {
        self.appServerClient = appServerClient
        
        self.firstname.accept(phoneCellViewModel.firstname)
        self.lastname.accept(phoneCellViewModel.lastname)
        self.phonenumber.accept(phoneCellViewModel.phonenumber)
        self.phoneId = phoneCellViewModel.id
        
        subscribeSubmitButtonTapped()
    }
    
    private func subscribeSubmitButtonTapped() {
        self.submitButtonTapped.asObserver()
            .subscribe(onNext: { [weak self] in
                self?.submitPhone()
                }
        ).disposed(by: disposeBag)
    }
    
    private func submitPhone() {
        loadInProgress.accept(true)
        
        appServerClient.patchPhone(
            firstname: firstname.value,
            lastname: lastname.value,
            phonenumber: phonenumber.value,
            id: phoneId)
            .subscribe(
                onNext: { [weak self] phone in
                    self?.loadInProgress.accept(false)
                    self?.onNavigateBack.onNext(())
                },
                onError: { [weak self] error in
                    self?.loadInProgress.accept(false)
                    let okAlert = SingleButtonAlert(
                        title: (error as? AppServerClient.PatchPhonesFailureReason)?.getErrorMessage() ?? "Could not connect to server.",
                        message: "Failed to update information.",
                        action: AlertAction(buttonTitle: "OK", handler: { print("Ok pressed!") })
                    )
                    self?.onShowError.onNext(okAlert)
                }
        )
            .disposed(by: disposeBag)
    }
}

fileprivate extension AppServerClient.PatchPhonesFailureReason {
    func getErrorMessage() -> String? {
        switch self {
        case .unAuthorized:
            return "Please login to update phones."
        case .notFound:
            return "Failed to update phone. Please try again."
        }
    }
}

