//
//  AddPhoneViewModel.swift
//  MyApp
//
//  Created by Дмитрий Заводнов on 6/23/20.
//  Copyright © 2020 Дмитрий Заводнов. All rights reserved.
//

import RxSwift
import RxCocoa

protocol AddPhoneViewModelProtocol {
    var title: BehaviorRelay<String> { get }
    var firstname: BehaviorRelay<String> { get }
    var lastname: BehaviorRelay<String> { get }
    var phonenumber: BehaviorRelay<String> { get }
    var submitButtonTapped: PublishSubject<Void> { get }
    var onShowLoadingHud: Observable<Bool> { get }
    var submitButtonEnabled: Observable<Bool> { get }
    var onNavigateBack: PublishSubject<Void>  { get }
    var onShowError: PublishSubject<SingleButtonAlert>  { get }
}

final class AddPhoneViewModel: AddPhoneViewModelProtocol {
    private let appServerClient: AppServerClientProtocol
    
    let onNavigateBack = PublishSubject<Void>()
    let onShowError = PublishSubject<SingleButtonAlert>()
    let submitButtonTapped = PublishSubject<Void>()
    
    var title = BehaviorRelay<String>(value: "Add Phone")
    var firstname = BehaviorRelay<String>(value: "")
    var lastname = BehaviorRelay<String>(value: "")
    var phonenumber = BehaviorRelay<String>(value: "")
    var onShowLoadingHud: Observable<Bool> {
        return loadInProgress
            .asObservable()
            .distinctUntilChanged()
    }
    
    var submitButtonEnabled: Observable<Bool> {
        return Observable.combineLatest(firstnameValid, lastnameValid, phoneNumberValid) { $0 && $1 && $2 }
    }
    
    private let loadInProgress = BehaviorRelay<Bool>(value: false)
    private let disposeBag = DisposeBag()
    
    private var firstnameValid: Observable<Bool> {
        return firstname.asObservable().map { $0.count > 0 }
    }
    private var lastnameValid: Observable<Bool> {
        return lastname.asObservable().map { $0.count > 0 }
    }
    private var phoneNumberValid: Observable<Bool> {
        return phonenumber.asObservable().map { $0.count > 0 }
    }
    
    init(appServerClient: AppServerClientProtocol) {
        self.appServerClient = appServerClient
        
        subscribeSubmitButtonTapped()
    }
    
    private func subscribeSubmitButtonTapped() {
        submitButtonTapped
            .subscribe(
                onNext: { [weak self] in
                    self?.postPhone()
                }
        )
            .disposed(by: disposeBag)
    }
    
    private func postPhone() {
        loadInProgress.accept(true)
        appServerClient.postPhones(
            firstname: firstname.value,
            lastname: lastname.value,
            phonenumber: phonenumber.value)
            .subscribe(
                onNext: { [weak self] _ in
                    self?.loadInProgress.accept(false)
                    self?.onNavigateBack.onNext(())
                },
                onError: { [weak self] error in
                    guard let `self` = self else {
                        return
                    }
                    
                    self.loadInProgress.accept(false)
                    
                    let okAlert = SingleButtonAlert(
                        title: (error as? AppServerClient.PostPhonesFailureReason)?.getErrorMessage() ?? " Not connect to server. Check your network.",
                        message: "Could not add \(self.firstname.value) \(self.lastname.value).",
                        action: AlertAction(buttonTitle: "OK", handler: { print("Ok pressed!") })
                    )
                    self.onShowError.onNext(okAlert)
                }
        )
            .disposed(by: disposeBag)
    }
}

private extension AppServerClient.PostPhonesFailureReason {
    func getErrorMessage() -> String? {
        switch self {
        case .unAuthorized:
            return "Please login to add Phones."
        case .notFound:
            return "Failed to add Phones. Please try again."
        }
    }
}

