//
//  AddPhoneViewController.swift
//  MyApp
//
//  Created by Дмитрий Заводнов on 6/23/20.
//  Copyright © 2020 Дмитрий Заводнов. All rights reserved.
//

import UIKit
import PKHUD
import RxSwift
import RxSwiftExt
import RxCocoa

final class AddPhoneViewController: UIViewController {

    @IBOutlet private weak var firstNameTextField: UITextField!
    @IBOutlet private weak var lastNameTextField: UITextField!
    @IBOutlet private weak var phoneNumberTextField: UITextField!
    @IBOutlet private weak var submitButton: UIButton!

    var viewModel: AddPhoneViewModelProtocol?
    var updatePhones = PublishSubject<Void>()

    let disposeBag = DisposeBag()

    private var activeTextField: UITextField?

    override func viewDidLoad() {
        super.viewDidLoad()
        bindViewModel()
    }

    override func viewWillDisappear(_ animated: Bool) {
        updatePhones.onCompleted()

        super.viewWillDisappear(animated)
    }

    private func bindViewModel() {
        guard let viewModel = viewModel else {
            return
        }

        title = viewModel.title.value

        bind(textField: firstNameTextField, to: viewModel.firstname)
        bind(textField: lastNameTextField, to: viewModel.lastname)
        bind(textField: phoneNumberTextField, to: viewModel.phonenumber)

        viewModel.submitButtonEnabled
        .bind(to: submitButton.rx.isEnabled)
        .disposed(by: disposeBag)

        submitButton.rx.tap.asObservable()
            .bind(to: viewModel.submitButtonTapped)
            .disposed(by: disposeBag)

        viewModel
            .onShowLoadingHud
            .map { [weak self] in self?.setLoadingHud(visible: $0) }
            .subscribe()
            .disposed(by: disposeBag)

        viewModel
            .onNavigateBack
            .subscribe(
                onNext: { [weak self] in
                    self?.updatePhones.onNext(())
                    let _ = self?.navigationController?.popViewController(animated: true)
                }
            ).disposed(by: disposeBag)

        viewModel
            .onShowError
            .map { [weak self] in self?.presentSingleButtonDialog(alert: $0)}
            .subscribe()
            .disposed(by: disposeBag)
    }

    private func bind(textField: UITextField, to behaviorRelay: BehaviorRelay<String>) {
        behaviorRelay.asObservable()
            .bind(to: textField.rx.text)
            .disposed(by: disposeBag)
        textField.rx.text.unwrap()
            .bind(to: behaviorRelay)
            .disposed(by: disposeBag)
    }

    private func setLoadingHud(visible: Bool) {
        PKHUD.sharedHUD.contentView = PKHUDSystemActivityIndicatorView()
        visible ? PKHUD.sharedHUD.show(onView: view) : PKHUD.sharedHUD.hide()
    }
}

// MARK: - Actions
extension AddPhoneViewController {
    @IBAction private func rootViewTapped(_ sender: Any) {
        activeTextField?.resignFirstResponder()
    }
}

// MARK: - UITextFieldDelegate
extension AddPhoneViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return false
    }

    func textFieldDidBeginEditing(_ textField: UITextField) {
        activeTextField = textField
    }

    func textFieldDidEndEditing(_ textField: UITextField) {
        activeTextField = nil
    }
}

extension AddPhoneViewController: SingleButtonDialogPresenter { }

