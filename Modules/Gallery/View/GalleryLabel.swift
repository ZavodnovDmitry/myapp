//
//  GalleryLabel.swift
//  MyApp
//
//  Created by Дмитрий Заводнов on 6/1/20.
//  Copyright © 2020 Дмитрий Заводнов. All rights reserved.
//

import Foundation
import UIKit

final class GalleryLabel: UILabel {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        translatesAutoresizingMaskIntoConstraints = false
        font = UIFont(name: "Arial-BoldMT", size: 26)
        textAlignment = .center
        textColor = .white
        shadowColor = .black
        shadowOffset = .init(width: 2, height: 0)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
