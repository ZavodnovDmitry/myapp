//
//  GalleryCollectionViewCell.swift
//  MyApp
//
//  Created by Дмитрий Заводнов on 5/30/20.
//  Copyright © 2020 Дмитрий Заводнов. All rights reserved.
//

import Foundation
import UIKit

final class GalleryCollectionViewCell: UICollectionViewCell {
    static let galleryIdCell = "galleryID"
    let galleryLable = GalleryLabel()
    
    let galleryImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        addSubview(galleryImageView)
        setupConstraintsForImageView()
        
        galleryImageView.addSubview(galleryLable)
        setupConstraintsForLabel()
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        self.layer.cornerRadius = self.frame.width / 4
        self.clipsToBounds = false
    }
    
    //MARK: - Setup
    private func setupConstraintsForImageView() {
        galleryImageView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20).isActive = true
        galleryImageView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -20).isActive = true
        galleryImageView.topAnchor.constraint(equalTo: topAnchor, constant: 20).isActive = true
        galleryImageView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -20).isActive = true
    }
    
    private func setupConstraintsForLabel() {
        galleryLable.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20).isActive = true
        galleryLable.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -20).isActive = true
        galleryLable.heightAnchor.constraint(equalTo: heightAnchor, multiplier: 0.1).isActive = true
        galleryLable.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -20).isActive = true
    }
    
}
