//
//  GalleryViewController.swift
//  MyApp
//
//  Created by Дмитрий Заводнов on 5/28/20.
//  Copyright © 2020 Дмитрий Заводнов. All rights reserved.
//

import UIKit

final class GalleryViewController: UIViewController {
    private let galleryCollectionView: GalleryCollectionView
    private var galleryTitle: String?
    
    init(galleryCollectionView: GalleryCollectionView,
         title: String) {
        self.galleryCollectionView = galleryCollectionView
        self.galleryTitle = title
        
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = self.galleryTitle
        
        addSubview()
        setupGalleryCollectionViewConstraints()
    }
    
    //MARK: - Setup
    private func addSubview() {
        view.addSubview(galleryCollectionView)
    }
    
    private func setupGalleryCollectionViewConstraints() {
        galleryCollectionView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        galleryCollectionView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        galleryCollectionView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        galleryCollectionView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
    }
    
}
