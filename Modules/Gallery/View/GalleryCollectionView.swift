//
//  GalleryCollectionView.swift
//  MyApp
//
//  Created by Дмитрий Заводнов on 5/30/20.
//  Copyright © 2020 Дмитрий Заводнов. All rights reserved.
//

import Foundation
import UIKit

final class GalleryCollectionView: UICollectionView {
    private let  galleryModel: [GalleryModel]
    private let galleryRouter: GalleryRouterProtocol
    private let layout = UICollectionViewFlowLayout()
    
    init(galleryModel: [GalleryModel],
         galleryRouter: GalleryRouterProtocol) {
        self.galleryModel = galleryModel
        self.galleryRouter = galleryRouter
        
        super.init(frame: .zero, collectionViewLayout: layout)
        settingsForGalleryCollectionView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: - Settings GalleryCollectionView
    private func settingsForGalleryCollectionView() {
        register(GalleryCollectionViewCell.self, forCellWithReuseIdentifier: GalleryCollectionViewCell.galleryIdCell)
        translatesAutoresizingMaskIntoConstraints = false
        showsHorizontalScrollIndicator = false
        showsVerticalScrollIndicator = false
        delegate = self
        dataSource = self
        contentInset = UIEdgeInsets(top: Constraints.zero,
                                    left: Constraints.GalleryConstraints.contentInsetLeft,
                                    bottom: Constraints.zero,
                                    right: Constraints.GalleryConstraints.contentInsetRight)
        layout.scrollDirection = .horizontal
        layout.minimumLineSpacing = Constraints.GalleryConstraints.minimumLineSpacing
        backgroundView = UIImageView(image: UIImage(named: "galleryBackground"))
    }
    
}

//MARK: - UICollectionViewDelegate, UICollectionViewDataSource
extension GalleryCollectionView: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return galleryModel.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: GalleryCollectionViewCell.galleryIdCell, for: indexPath) as! GalleryCollectionViewCell
        
        cell.galleryImageView.image = galleryModel[indexPath.row].image
        cell.galleryLable.text = galleryModel[indexPath.row].title
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if galleryModel[indexPath.row].title == String.GalleryString.notesVCTitle {
            galleryRouter.pushNotebookVC()
        } else if galleryModel[indexPath.row].title == String.GalleryString.horoscopeVCTitle {
            galleryRouter.pushHoroscopeVC()
        } else if galleryModel[indexPath.row].title == String.GalleryString.phoneVCTitle {
            galleryRouter.pushPhoneVC()
        }
    }
}

//MARK: - UICollectionViewDelegateFlowLayout
extension GalleryCollectionView: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: Constraints.GalleryConstraints.galleryItemWidth, height: Constraints.GalleryConstraints.galleryItemHeight)
    }
}
