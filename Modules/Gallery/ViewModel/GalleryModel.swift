//
//  GalleryModel.swift
//  MyApp
//
//  Created by Дмитрий Заводнов on 5/30/20.
//  Copyright © 2020 Дмитрий Заводнов. All rights reserved.
//

import Foundation
import UIKit

struct GalleryModel {
    let title: String
    let image: UIImage
    
    static func createGalleryModel() -> [GalleryModel] {
        
        let notesImage = UIImage(named: "notes") ?? UIImage()
        let horoscopeImage = UIImage(named: "question") ?? UIImage()
        let phoneImage = UIImage(named: "contact") ?? UIImage()
        
        let notesScreen = GalleryModel(title: String.GalleryString.notesVCTitle, image: notesImage)
        let horoscopeScreen = GalleryModel(title: String.GalleryString.horoscopeVCTitle, image: horoscopeImage)
        let phoneScreen = GalleryModel(title: String.GalleryString.phoneVCTitle, image: phoneImage)
        
        return [notesScreen, phoneScreen, horoscopeScreen]
    }
}
