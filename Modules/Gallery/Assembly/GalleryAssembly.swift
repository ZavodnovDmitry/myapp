//
//  GalleryAssembly.swift
//  MyApp
//
//  Created by Дмитрий Заводнов on 5/31/20.
//  Copyright © 2020 Дмитрий Заводнов. All rights reserved.
//

import Foundation
import UIKit

final class GalleryAssembly {
    
    func assemblyGallery() -> GalleryViewController {
        let galleryModel = GalleryModel.createGalleryModel()
        let galleryRouter = GalleryRouter()
        let galleryCollectionView = GalleryCollectionView(galleryModel: galleryModel,
                                                          galleryRouter: galleryRouter)
        let galleryVC = GalleryViewController(galleryCollectionView: galleryCollectionView,
                                              title: String.GalleryString.titleGalleryVC)
        return galleryVC
    }
}
