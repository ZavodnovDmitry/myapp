//
//  GalleryRouter.swift
//  MyApp
//
//  Created by Дмитрий Заводнов on 6/1/20.
//  Copyright © 2020 Дмитрий Заводнов. All rights reserved.
//

import Foundation
import UIKit

protocol GalleryRouterProtocol {
    func pushNotebookVC()
    func pushHoroscopeVC()
    func pushPhoneVC()
}

final class GalleryRouter: GalleryRouterProtocol {
    
    func pushNotebookVC() {
        let navigationVC = MainTabBarController.navigationVC
        let notebookVC = NotebookAssembly().assemblyNotebook()
        navigationVC.pushViewController(notebookVC, animated: true)
    }
    
    func pushHoroscopeVC() {
        let navigationVC = MainTabBarController.navigationVC
        let horoscopeVC = HoroscopeViewController.instantiateInitialVC()
        navigationVC.pushViewController(horoscopeVC, animated: true)
    }
    
    func pushPhoneVC() {
        let navigationVC = MainTabBarController.navigationVC
        let phoneVC = PhoneAssembly().assemblyPhone()
        navigationVC.pushViewController(phoneVC, animated: true)
    }
}
