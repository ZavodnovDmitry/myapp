//
//  MainTabBarController.swift
//  MyApp
//
//  Created by Дмитрий Заводнов on 5/28/20.
//  Copyright © 2020 Дмитрий Заводнов. All rights reserved.
//

import UIKit

final class MainTabBarController: UITabBarController {
    static var navigationVC = UINavigationController()
    private let galleryVC = GalleryAssembly().assemblyGallery()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewControllers = [assembleViewController(viewController: galleryVC,
                                                  image: String.GalleryString.imageGalleryVC,
                                                  title: String.GalleryString.titleGalleryVC)]
    }
    
    private func assembleViewController(viewController: UIViewController,
                                        image: String,
                                        title: String) -> UIViewController {
        
        MainTabBarController.navigationVC = UINavigationController(rootViewController: viewController)
        let image = UIImage(systemName: image)
        MainTabBarController.navigationVC.tabBarItem.image = image
        MainTabBarController.navigationVC.tabBarItem.title = title
        
        return MainTabBarController.navigationVC
    }
    
}
